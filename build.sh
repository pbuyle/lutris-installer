#!/bin/bash

PATH=$PATH:./bin

command_exists() {
    # check if command exists and fail otherwise
    command -v "$1" >/dev/null 2>&1
    if [[ $? -ne 0 ]]; then
        echo "$1 required but it's not installed. Abort."
        exit 1
    fi
}

command_exists "jsonnet"
command_exists "yq"

mkdir -p dist
jsonnet -y src/jsonnet/index.jsonnet | yq -P -s '["dist/", .game_slug, "-", .slug, ".yml"] | join("")' --no-doc
