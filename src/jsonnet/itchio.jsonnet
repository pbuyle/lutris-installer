{
  name: "itch.io",
  game_slug: "itchio",
  version: "latest (itch-setup)",
  slug: "itch-setup-latest",
  runner: "linux",

  script: {
    game: {
      exe: "$GAMEDIR/itch",
      working_dir: "$GAMEDIR",
    },
    files: [
      { "itch-setup": {
          filename: "itch-setup",
          url: "https://itch.io/app/download?platform=linux"
      } }
    ],
    installer: [
      { chmodx: "itch-setup"},
      # Replace $GAMEDIR with a synlink to $HOME/.itch to integrate with itch.io running without Lutris.
      # This re-use an existing itch.io install
      { execute: { command: 'mkdir -p $HOME/.itch && rm -rf $GAMEDIR && ln -s $HOME/.itch $GAMEDIR' } },
      { execute: { file: "itch-setup", args: "--silent", disable_runtime: true } }
    ]
  }
}