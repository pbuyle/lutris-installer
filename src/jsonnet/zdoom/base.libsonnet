{
   /**
    * Define a installer to merge into the base Doom II installer.
    *
    * To use, simply provide a standard installer as input, with only the specifics parts set.
    * See existing .jsonnet file in `doom2` folder for    
    *
    * The base Doom II installer will
    * - Set (or override if set) `runner: zdoom"
    * - Set ((or override if set) `script.game`'s  `working_dir`, `main_file` (DOOM2.WAD) and `save_dir`
    * - Add a first directive to display an 'Insert disc' dialog to locate DOOM2.WAD
    * - Add a last directive to creare the save dir (`$GAMEDIR/saves`)
    */
   doom2(base):: base {
      runner: "zdoom",
      script+: {
         game+: {
            "working_dir": "$GAMEDIR",
            "main_file": "$DISC/DOOM2.WAD",
            "savedir": "$GAMEDIR/saves",
         },
         installer: std.flattenArrays([
            [{
               "insert-disc": {
                  "description": "Please browse folder where is located DOOM2.WAD file. Autodetect will not work here, use \"Browse\" button.",
                  "requires": "DOOM2.WAD"
               }
            }],
            base.script.installer,
            [{
                  "execute": {
                     "command": "mkdir -p \"$GAMEDIR/saves\""
                  }
            }]
         ])
      }
   }
}
