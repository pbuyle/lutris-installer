local base = import 'base.libsonnet';
base.doom2({
   name: "Elementalism",
   game_slug: "elementalism",
   version: "Phase 1 Full Release v1.2",
   slug: "phase1-full-v1.2",
   script: {
      game: {
         files: [
            "$GAMEDIR/Elementalism_Phase1_Full_Release_v1.2.pk3"
         ]
      },
      files: [
         {
            "elementalism": "N/A:Select `Elementalism_Phase1_Full_Release_v1.2.zip` downloaded from https://www.moddb.com/mods/elementalism/downloads/elementalism-phase1"
         }
      ],
      installer: [
         {
            extract: {
               file: "elementalism",
               dst: "$GAMEDIR",
            }
         },
      ]
   }
})