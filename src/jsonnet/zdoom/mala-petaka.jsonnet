{
  name: "Mala Petaka",
  game_slug: "mala-petaka",
  version: "Demo v0.3",
  slug: "demo-v0.3",
  runner: "zdoom",

  script: {
    game: {
      working_dir: "$GAMEDIR",
      main_file: "$GAMEDIR/malapetaka_demo.ipk3",
      savedir: "$GAMEDIR/saves",
    },
    files: [
      { dist: "N/A:Select `Mala Petaka v0.4.zip` downloaded from https://sanditiojitok.itch.io/mala-petaka-demo/purchase" }
    ],
    installer: [
      { extract: { file: "dist", dst: "$CACHE/dist" } },
      { move: { src: "$CACHE/dist/malapetaka_demo.ipk3", dst: "$GAMEDIR/malapetaka_demo.ipk3" } },
      { execute: { command: 'mkdir "$GAMEDIR/saves"' } },
    ]
  }
}
